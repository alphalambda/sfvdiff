from model_transform.bcm import cancel_list_id
from model_transform.util import read_flags, transform_time_element


def transform_type1(data):
    res = transform_time_element(data)
    res["State"] = read_flags(data["Flag1"], [
        "Stand", "Crouch", "Air", None,
        None, "Counter Hit", None, None,
        None, "No Corner Correction", None, None,
        None, None, None, "On Ground",

        "Auto-Correct", "Reverse Auto-Correct", None, None,
        None, None, None, None,
        None, None, None, "Extra Damage"
    ])
    if data["Flag2"]:
        res["Unknown"] = {
            "Flag2": read_flags(data["Flag2"])
        }
    return res


def transform_cancel(data, idx, move_list_id, cancel_list_usage):
    res = transform_time_element(data)
    res["CancelList"] = cancel_list_id(data["CancelList"])
    cancel_list_usage[res["CancelList"]].add((move_list_id, idx))
    if data["Type"] == 0:
        res["Type"] = "Buffer Always"
    elif data["Type"] < 8:
        res["Type"] = read_flags(data["Type"], ["Buffer on Hit", "Buffer on Block", "Buffer on Whiff"])
    else:
        res["Type"] = "Execute"
    return res


def transform_box(data):
    res = transform_time_element(data)
    res["X"] = data["X"]
    res["Y"] = data["Y"]
    res["Width"] = data["Width"]
    res["Height"] = data["Height"]
    return res

