def transform_bch(data):
    if "VT_Time1" not in data:
        # V1 version
        return {
            "Stun": data["StunGauge"],
            "Life": data["VitalGauge"],
            "VGauge": data["VGauge"],
            "VTrigger": data.get("VT1_Time"),
        }

    res = {
        "Stun": data["StunGauge"],
        "Life": data["VitalGauge"],
        "VGauge": data.get("VGauge0", data.get("VGauge")),
        "VTrigger": data.get("VT_Time0", data.get("VT1_Time", data.get("VT_Time1"))),
        "VGauge2": data.get("VGauge1"),
        "VTrigger2": data.get("VT_Time1"),
    }
    return {k: v for k, v in res.items() if v is not None}
