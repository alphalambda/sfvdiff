import struct

from model_transform.util import read_flags


def better_hit_effect(hit_effect):
    parts = hit_effect.split("_")
    if parts[1] == "UNKNOWN":
        parts[1] = "OTG"
    return "_".join(parts)


def transform_hitbox_effect(data, index_):
    res = {
        "Type": ["No Effect", "Hit", "Guard", "Blow", "OTG"][data["Type"]],
        "Script": (data["Index"] if data["Index"] >= 3 else ["L", "M", "H"][data["Index"]]) if data["Type"] > 0 else None,
        "Flag": read_flags(
            data["DamageType"],
            [
                "Specific Script", "Don't Kill", None, "Hard Knockdown",
                None, "Crush Counter", "White Damage", None,
                None, None, None, None,
                "No Back Recovery", "Screenshake"
            ]
        ) or None,
        "Damage": data["Damage"] or None,
        "Stun": data["Stun"] or None,
        "EXAttacker": data["EXBuildAttacker"] or None,
        "EXDefender": data["EXBuildDefender"] or None,
        "VTimer": (data["Index9"] >> 16) or None,
        "VGauge": data["Index12"] or None,
        "HitFreezeAttacker": (data["HitStunFramesAttacker"] & 0xFF) or None,
        "HitFreezeDefender": (data["HitStunFramesDefender"] & 0xFF) or None,
        "MainRecoveryDuration": data["RecoveryAnimationFramesDefender"],
        "KnockdownDuration": data["Index17"],
        "KnockBack": data["KnockBack"],
        "FallSpeed": data["FallSpeed"],
        "JuggleStart": data["Index18"] & 0xFF,
        "DamageScalingGroup": data["Index19"] if data["Index19"] != 0 else None
    }
    unk = dict()
    if data["HitStunFramesAttacker"] >> 8:
        unk["HitStunFramesAttacker_HI"] = data["HitStunFramesAttacker"] >> 8
    if data["HitStunFramesDefender"] >> 8:
        unk["HitStunFramesDefender_HI"] = data["HitStunFramesDefender"] >> 8
    if data["Index18"] >> 8:
        unk["Index18_HI"] = read_flags(data["Index18"] >> 8)
    if data["Index9"] & 0xFFFF:
        unk["Index9_LO"] = data["Index9"] & 0xFFFF
    if data["Index22"]:
        unk["Index22"] = struct.unpack("f", struct.pack("i", data["Index22"]))[0]
    if data["Index23"]:
        unk["Index23"] = data["Index23"]
    if data["Index24"]:
        unk["Index24"] = data["Index24"]
    if data["FuzzyEffect"]:
        unk["FuzzyEffect"] = read_flags(data["FuzzyEffect"])
    res["Unknown"] = unk
    return {k: v for k, v in res.items() if v is not None}
