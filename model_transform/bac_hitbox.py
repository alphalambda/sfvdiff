from model_transform.bac_misc import transform_box
from model_transform.util import read_flags

KNOWN_CANT_HIT = (0x60 + 0x7)


def transform_hitbox(data):
    res = transform_box(data)
    unk = dict()
    if data["Unknown6"] & KNOWN_CANT_HIT > 0:
        res["CantHit"] = read_flags(data["Unknown6"] & KNOWN_CANT_HIT, ["Standing", "Crouched", "Air", None, None, "Front", "Cross-Up"])
    uk6 = data["Unknown6"] - (data["Unknown6"] & KNOWN_CANT_HIT)
    if uk6 > 0:
        unk["Unknown6"] = read_flags(uk6)
    if data["Unknown7"] > 0:
        res["Flag"] = read_flags(data["Unknown7"], [
            None, "Armor Break", None, None,
            "No Back Recovery", None, None, None,
            "Combo Throw", "Ignore?", "Force Multi-Hit Counter"
        ])
    res["Group"] = data["Unknown8"] & 0xFF
    res["Level"] = ["Middle", "High", "Low"][data["NumberOfHits"] >> 8]
    res["HitType"] = ["Strike", "Projectile", "Throw", "Unknown Type3", "Proximity Guard", "Projectile Cancel", "Unknown Type6"][data["HitType"]]

    if data["HitType"] < 4:
        res["Strength"] = data["Unknown8"] >> 8
        res["NumberOfHits"] = data["NumberOfHits"] & 0xFF
        res["JuggleLimit"] = data["JuggleLimit"]
        res["JuggleIncrease"] = data["JuggleIncrease"]
        res["HitRange"] = ["Standard", "Near", "Far"][data["Unknown10"] // 16]
    if data["HitboxEffectIndex"] >= 0:
        res["Effect"] = data["HitboxEffectIndex"]
    if data["Flag4"]:
        unk["Flag4"] = read_flags(data["Flag4"])
    if data["Unknown11"]:
        unk["Unknown11"] = read_flags(data["Unknown11"])
    if data.get("Unknown12"):
        unk["Unknown12"] = read_flags(data["Unknown12"])
    res["Unknown"] = unk
    return res
