from model_transform.bac_autocancel import transform_autocancel
from model_transform.bac_force import transform_force
from model_transform.bac_hitbox import transform_hitbox
from model_transform.bac_hitbox_effect import better_hit_effect, transform_hitbox_effect
from model_transform.bac_hurtbox import transform_hurtbox
from model_transform.bac_misc import transform_type1, transform_cancel, transform_box
from model_transform.bac_other import transform_other
from model_transform.bac_position import transform_positions
from model_transform.util import read_flags, transform_time_element


def transform_script(data, move_list_id, cancel_list_usage):
    if data is None:
        return None
    idx = data["Index"]
    res = {
        "Index": idx,
        "Name": data["Name"],
        "LastActionFrame": data["LastHitboxFrame"] if data["LastHitboxFrame"] >= 0 else None,
        "InterruptFrame": data["InterruptFrame"] if data["InterruptFrame"] >= 0 else None,
        "TotalTicks": data["TotalTicks"],
        "Flag": read_flags(data["Flag"], [
            "Starts Crouched", "Air", "Bound", None,
            None, None, None, None,
            None, None, None, None,
            None, None, None, None,
            "Ends Crouched", "Landing", "Knockdown"
        ]) if data["Flag"] else None,
        "SlideVX": data["Slide"] if data["Slide"] != 0 else None,
        "SlideVY": data["unk3"] if data["unk3"] != 0 else None,
        "SlideAX": data["unk5"] if data["unk5"] != 0 else None,
        "SlideAY": data["unk6"] if data["unk6"] != 0 else None
    }
    unk = {
        "ReturnToOriginalPosition": read_flags(data["ReturnToOriginalPosition"])
    }
    if data["unk9"]:
        unk["unk9"] = data["unk9"]
    if data["unk13"]:
        unk["unk13"] = data["unk13"]
    if data["HeaderSize"] > 64:
        for i in range(12, 23):
            if data["Unknown%d" % i] > 0:
                unk["Unknown%d" % i] = data["Unknown%d" % i]
    res["Unknown"] = unk

    res["Status"] = [transform_type1(t) for t in data["Type1s"] or []] or None
    res["AutoCancels"] = [transform_autocancel(t) for t in data["AutoCancels"] or []] or None
    res["Cancels"] = [transform_cancel(t, idx, move_list_id, cancel_list_usage) for t in data["Cancels"] or [] if t["CancelList"] >= 0] or None
    res["Hitboxes"] = [transform_hitbox(t) for t in data["Hitboxes"] or []] or None
    res["Hurtboxes"] = [transform_hurtbox(t) for t in data["Hurtboxes"] or []] or None
    res["Pushboxes"] = [transform_box(t) for t in data["PhysicsBoxes"] or []] or None
    res["Forces"] = [transform_force(t) for t in data["Forces"] or []] or None
    res["Positions"] = transform_positions(data["Positions"], data["TotalTicks"])
    res["Commands"] = [transform_other(t) for t in data["Others"] or [] if
                       t["Unknown1"] == 1 or (t["Unknown1"] == 0 and 0 < t["Unknown2"] <= 12)] or None
    unk["Others"] = [transform_other(t) for t in data["Others"] or [] if
                     t["Unknown1"] > 1 or (t["Unknown1"] == 0 and (t["Unknown2"] == 0 or t["Unknown2"] > 12))] or None
    if unk["Others"] is None:
        del unk["Others"]

    res = {k: v for k, v in res.items() if v is not None}
    return res


def transform_bac(data, cancel_lists_usage):
    scripts = [{m["Index"]: transform_script(m, i, cancel_lists_usage) for m in ml["Moves"] if m is not None} for i, ml in enumerate(data["MoveLists"])]
    return {
        "Scripts": {
            "Normal": scripts[0]
        } if len(scripts) == 1 else {
            "Normal": scripts[0],
            "Alternate": scripts[1],
        },
        "HitboxEffects": {
            he["Index"]: {
                better_hit_effect(situation): transform_hitbox_effect(content, he["Index"])
                for situation, content in
                he.items() if situation != "Index"
            }
            for he in data["HitboxEffectses"] if he["HIT_STAND"]
        }
    }

