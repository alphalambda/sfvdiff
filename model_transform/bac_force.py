from model_transform.util import transform_time_element


def transform_force(data):
    res = transform_time_element(data)
    flag = data["Flag"]
    if flag == "HorizontalSpeed":
        flag = 0x1
    elif flag == "VerticalSpeed":
        flag = 0x10
    elif flag == "HorizontalAcceleration":
        flag = 0x1000
    elif flag == "VerticalAcceleration":
        flag = 0x10000
    if flag == 0:
        type_ = 0
        flag = 0
    elif flag & 0xf:
        type_ = 1
    elif flag & 0xf0:
        type_ = 2
        flag = flag >> 4
    elif flag & 0xf00:
        type_ = 3
        flag = flag >> 8
    elif flag & 0xf000:
        type_ = 4
        flag = flag >> 12
    elif flag & 0xf0000:
        type_ = 5
        flag = flag >> 16
    else:
        type_ = 6
        flag = flag >> 20
    res["Amount"] = data["Amount"]
    res["Target"] = ["None", "Horizontal Speed", "Vertical Speed", "Ignored (Type3)", "Horizontal Acceleration", "Vertical Acceleration", "Ignored (Type6)"][type_]
    if flag & 8:
        res["Mode"] = "Temporary"
    elif flag & 4:
        res["Mode"] = "On Speed Change"
    elif flag & 2:
        res["Mode"] = "Multiply"
    else:
        res["Mode"] = "Set"
    return res
