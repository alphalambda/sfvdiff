from model_transform.util import read_flags
import struct

def transform_charge(data):
    return {
        "Index": data["Index"],
        "InputMatch": read_input_properties(data["ChargeDirection"], data["Unknown1"]),
        "Frames": data["ChargeFrames"],
        "Unknown": {"Flags": read_flags(data["Flags"])}
    }


def read_input_properties(input_mask, input_prop):
    direction = input_prop & 7
    button = (input_prop >> 4) & 7
    unk = (input_prop >> 8) & 7
    edge = (input_prop >> 12) & 7
    res = {
        "Input": read_input_mask(input_mask),
        "Direction": {0: "None", 1: "Similar", 2: "Exact"}.get(direction, "0x%x" % direction),
        "Button": {0: "None", 1: "Any", 2: "All", 3: "Two"}.get(button, "0x%x" % button)
    }
    if edge:
        res["Edge"] = read_flags(edge, ["Positive", None, "Negative"])
    if unk:
        res["Unknown"] = read_flags(unk)
    return res


def read_input_mask(input_mask):
    return read_flags(input_mask, [
        "UP", "DOWN", "BACK", "FORWARD", "LP", "MP", "HP", "LK", "MK", "HK", None, None, "NEUTRAL"
    ])


def transform_input(data):
    if data["Name"] is None:
        return None
    return list(map(transform_input_entry, data["InputEntries"][2]["InputParts"]))


def transform_input_entry(data):
    input_type = data["InputType"]
    if input_type == "Normal":
        input_type = 0
    elif input_type == "Charge":
        input_type = 1
    if input_type == 0:
        return {
            "Type": "Normal",
            "InputMatch": read_input_properties(data["Input"], data["Unknown1"]),
            "Buffer": data["Buffer"],
        }
    elif input_type == 1:
        return {
            "Type": "Charge",
            "ChargeIndex": data["Input"]
        }
    elif input_type == 2:
        return {
            "Type": "360" if data["Input"] == 1 else "720"
        }
    elif input_type == 3:
        return {
            "Type": "Impulse",
            "InputMatch": read_input_properties(data["Input"], data["Unknown1"]),
            "Buffer": data["Buffer"],
        }
    else:
        return {
            "Type": "Unknown"
        }


def transform_cancel_list(data):
    return [e["Name"] for e in data["Cancels"]]


def transform_move(data, max_uk8):
    input_match = read_input_properties(data["Input"], data["InputFlags"])
    if data["InputMotionIndex"] >= 0:
        input_match["MotionIndex"] = data["InputMotionIndex"]
    projectile_limit = struct.unpack('BBBB', struct.pack('i', data["ProjectileLimit"]))
    res = {
        "Index": data["Index"],
        "Name": data["Name"],
        "Script": data["ScriptIndex"],
        "InputMatch": input_match,
        "HeightRestriction": data["RestrictionDistance"] if data["PositionRestriction"] > 0 else None,
        "BloodyGarden": data["Unknown4"] > 0 if data["Unknown4"] > 0 else None,
        "ProjectileLimit": projectile_limit[0] if projectile_limit[0] > 0 else None,
        "ProjectileGroup": projectile_limit[1] if projectile_limit[1] > 0 else None,
        "Category": next(iter(read_flags(data["Unknown6"] & 0xFF, [None, "Command Attack", "Special", "Ex Special", "CA", "V-Skill", "V-Reversal", "V-Trigger"]))) if (data["Unknown6"] & 0xFF) > 0 else "Neutral",
        "SubCategories": read_flags(data["Unknown6"] >> 8, ["Light", "Medium", "Heavy", "Punch", "Kick", "Throw", "Projectile"]),
        "Stance": {1: "Normal", 2: "Alternative", 3: "Any"}.get(data["Unknown8"], "Unknown") if max_uk8 > 1 else None,
        "VMeterRequires": data["VtriggerRequirement"] if data["VtriggerRequirement"] != 0 else None,
        "VMeterConsumes": data["VtriggerUsed"] if data["VtriggerUsed"] != 0 else None,
        "EXRequires": data["MeterRequirement"] if data["MeterRequirement"] != 0 else None,
        "EXConsumes": data["MeterUsed"] if data["MeterUsed"] != 0 else None,
        "VTriggerConsumes": data["Unknown16"] >> 16,
        "VTriggerActivated": data["Unknown16"] & 0xFFFF if data["Unknown16"] & 0xFFFF else None,
        "VTriggerRequired": (projectile_limit[2] + 1) if projectile_limit[3] else None,
    }
    res = {k: v for k, v in res.items() if v is not None}
    unk = dict()
    if data["Unknown7"] > 0:
        unk["UK7-ResourceRestriction"] = read_flags(data["Unknown7"])
    data["Unknown17_HI"] = data["Unknown17"] >> 16
    data["Unknown17_LO"] = data["Unknown17"] & 0xFFFF
    if data["Unknown17_LO"] > 0:
        unk["UK17_LO"] = data["Unknown17_LO"]
    if data["Unknown17_HI"] > 0:
        unk["UK17_HI"] = data["Unknown17_HI"]
    if data["Unknown18"] > 0:
        unk["UK18"] = data["Unknown18"]
    if data["Unknown20"] > 0:
        unk["UK20"] = data["Unknown20"]
    if data["Unknown21"] > 0:
        unk["UK21"] = data["Unknown21"]
    if data["Unknown22"] > 0:
        unk["UK22"] = data["Unknown22"]
    if data["Unknown24"] > 0:
        unk["UK24"] = data["Unknown24"]
    if data["Unknown25"] > 0:
        unk["UK25"] = data["Unknown25"] > 0
    if data["Unknown26"] > 0:
        unk["UK26"] = data["Unknown26"]
    if data["NormalOrVtrigger"] > 0:
        unk["UK27"] = data["NormalOrVtrigger"]
    if data["Unknown28"] > 0:
        unk["UK28"] = data["Unknown28"]
    if len(unk) > 0:
        res["Unknown"] = unk

    return res


def cancel_list_id(idx):
    if idx < 4:
        return ["GROUND", "NEUTRAL JUMP", "FORWARD JUMP", "BACKWARD JUMP"][idx]
    return str(idx)


def transform_bcm(bcm):
    cancel_lists = dict()
    if bcm["CancelLists"]:
        cancel_lists = {
            cancel_list_id(i): transform_cancel_list(c)
            for i, c in enumerate(bcm["CancelLists"])
            if c["Cancels"] is not None
        }
    max_uk8 = max(m["Unknown8"] for m in bcm["Moves"])
    return {
        "Charges": {i: elem for i, elem in enumerate(map(transform_charge, bcm["Charges"]))},
        "Inputs": {i: elem for i, elem in enumerate(map(transform_input, bcm["Inputs"])) if elem is not None},
        "CancelLists": cancel_lists,
        "Moves": {m["Name"]: transform_move(m, max_uk8) for m in bcm["Moves"]}
    }


import bson, json

# basepath = "D:/Steam/SteamApps/common/StreetFighterV/StreetFighterV/Content/Paks/moves/StreetFighterV/Content/Chara"
# char_ = "Z25"
# char_path = basepath + "/" + char_ + "/BattleResource/" + "001"
# bcm = transform_bcm(bson.loads(open(char_path + "/BCM_" + char_ + ".json", mode='rb').read()))
# print(json.dumps(bcm, indent=4, sort_keys=True, default=lambda x: list(x)))
