def transform_time_element(data):
    res = {
        "TickStart": data["TickStart"],
        "TickEnd": data["TickEnd"]
    }
    flag1 = data.get("BACVERint1", 0)
    if flag1:
        res["ActiveOnStates"] = read_flags(flag1, ["A", "B", "C", "D", "E", "F", "G", "H"])
    flag2 = data.get("BACVERint2", 0)
    if flag2:
        res["InactiveOnStates"] = read_flags(flag2, ["A", "B", "C", "D", "E", "F", "G", "H"])
    flag3 = data.get("BACVERint3", 0)
    if flag3:
        res["TickFlag3"] = flag3
    flag4 = data.get("BACVERint4", 0)
    if flag4:
        res["TickFlag4"] = flag4
    return res


class Flag(frozenset):
    def __str__(self):
        return str(self.sorted)

    def __repr__(self):
        return self.__str__()

    def __iter__(self):
        return iter(self.sorted)


def read_flags(value, strings=None):
    res = []
    mask = 1
    while value > 0:
        if value & 1:
            res.append(strings[0] if strings and strings[0] else "0x%x" % mask)
        value >>= 1
        mask <<= 1
        if strings:
            strings = strings[1:]
    flag = Flag(res)
    flag.sorted = res
    return flag
