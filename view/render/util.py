
def extract_diff_for_view(diff):
    res = {k: (v["type"].split("_")[-1], v["change"]) for k, v in diff.get("#DIFF", {}).items()}
    for k in diff:
        if k == "#DIFF":
            continue
        res[k] = ("changed", None)
    return res
