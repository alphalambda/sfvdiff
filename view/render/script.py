import json

from view.force_simulation import simulate_positions
from view.render.box import render_boxes
from view.render.jinja import render
from view.render.routing import move_path
from view.render.timeline import render_all_timeline
from view.render.util import extract_diff_for_view
from view.render.version_menu import cancel_list_sort


def contains_unknown(diff):
    if type(diff) != dict:
        return False
    if "Unknown" in diff:
        return True
    for k, v in diff.items():
        if k == "#DIFF":
            if v.get("key", "").startswith("Unknown"):
                return True
            continue
        if contains_unknown(v):
            return True
    return False


def render_script(script, diff, old_version, menu_data, **kwargs):
    paths = ["Status", "Hitboxes", "Hurtboxes", "Pushboxes", "Cancels", "AutoCancels", "Commands", "Forces",
             "Unknown"]
    data = kwargs["data"]
    states = set()
    left_panel = render_all_timeline(script, paths, diff, states, **kwargs)
    right_panel, box_js = render_boxes(script, diff)
    positions = simulate_positions(script)
    old_positions = compute_old_positions(old_version, positions, script)
    property_diff = extract_diff_for_view(diff)
    append_property_diff(diff, property_diff, "Unknown")
    if "Unknown" not in property_diff:
        # check that the underlying timelines dont contain
        if contains_unknown(diff):
            property_diff["Unknown"] = True
    append_property_diff(diff, property_diff, "Positions")
    js = """
    var states = %s;
    var totalTicks = %d;
    var interruptFrame = %d;
    var positions = %s;
    var oldPositions = %s;
    %s
    """ % (
        str(sorted(states)),
        script["TotalTicks"],
        script.get("InterruptFrame", 10000),
        json.dumps(positions, default=float),
        json.dumps(old_positions, default=float),
        box_js
    )
    moves = data["BCM"]["Moves"]
    cancel_lists = [
        (cl, [(m, move_path(moves[m])) for m in data["BCM"]["CancelLists"].get(cl, [])])
        for cl in sorted({c["CancelList"] for c in script.get("Cancels", [])}, key=lambda x: cancel_list_sort(x))
    ]
    return render(
        'script.html',
        title="#%d - %s" % (script["Index"], script["Name"]),
        menu_data=menu_data, cancel_lists=cancel_lists,
        timeline=left_panel, boxes=right_panel, js=js, script=script,
        positions=positions, property_diff=property_diff
    )


def append_property_diff(diff, property_diff, key):
    if key in diff:
        property_diff[key] = "changed"
        for k, uk_diff in diff[key].get("#DIFF", {}).items():
            property_diff[key + "." + str(k)] = (uk_diff["type"].split("_")[-1], uk_diff["change"])
        for k, uk_diff in diff[key].items():
            property_diff[key + "." + str(k)] = ("changed", None)


def compute_old_positions(old_version, positions, script):
    if old_version:
        old_positions = simulate_positions(old_version)

        for k, v in list(old_positions.items()):
            if k not in positions:
                positions[k] = []
                continue
            pos = positions[k]
            if pos == v:
                old_positions[k] = []
                # remove positions that didnt change
            # for i in range(0, min(old_version["TotalTicks"], script["TotalTicks"])):
            #     if v[i] == pos[i]:
            #         v[i] = None
    else:
        old_positions = None
    return old_positions
