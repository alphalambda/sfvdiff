import json
import html
from collections import Iterable

from view.render.routing import he_path, script_path, get_script_routing

render_label = {
    "Status": { "State": {} },
    "Hurtboxes": {
        "Level": {"Middle": "Mid"},
        "Sensitivity": {"Projectile": "★", "Strike": "🤜", "Throw": "🖐"}
    },
    "Hitboxes": {
        "Level": {"Middle": "Normal", "High": "Overhead"},
        "HitType": {"Projectile": "★", "Strike": "🤜", "Throw": "🖐", "Proximity Guard": "Proximity"},
        "JuggleIncrease": "JugInc",
        "JuggleLimit": "JugLim",
        "HitRange": {}
    },
    "Cancels": {
        "Type": {}
    },
    "Commands": {
        "Command": {}
    },
    "Unknown": {
        "Command": {},
        "Params": {}
    }
}


def render_grid(ticks, interrupt, x_scale, action):
    return "".join([
        "<div class='grid%s' onclick='%s' style='left:%dpx;'>%s</div>" % (
            " interrupt" if interrupt is not None and interrupt < i else "",
            action(i),
            i * x_scale, i + 1
        )
        for i in range(0, ticks)
    ])


def format_value(value):
    if type(value) == float:
        return "%.06g" % value
    res = str(value)
    if len(res) > 300:
        res = res[:300] + "..."
    return html.escape(res)


def render_box_property(key, value, path, property_diff, additional_class, uk=False, **kwargs):
    # if key == "Unknown":
    #     return ""
    diff_for_property = property_diff.get("#DIFF", {}).get(key)
    label = label_render(key, path, value, **kwargs)
    tooltip = ""
    if not additional_class:
        additional_class = diff_for_property["type"].split('_')[-1] if diff_for_property else ""
        if additional_class == "changed":
            old_value = label_render(key, path, diff_for_property["change"]["old_value"], True, **kwargs)
            tooltip = " data-toggle='tooltip' data-placement='top' title='was %s'" % old_value
        if not diff_for_property and key in property_diff:
            additional_class = "changed"
            # TODO provide a tooltip?
    if uk:
        additional_class += " unknown"
    return "<span class='property %s'%s>%s</span>" % (additional_class, tooltip, label)


def label_render(key, path, value, tooltip=False, data=None, **kwargs):
    if not tooltip:
        if key == "Effect":
            return "Effect: <a href='../%s'>%d</a>" % (he_path(value, **kwargs), value)
        if key == "Script" and type(value) == int:
            route, script = get_script_routing(data, value, **kwargs)
            return "Script: <a href='../%s'>#%d - %s</a>" % (route, script["Index"], script["Name"])
    if key == "ScriptTime":
        value += 1
    render = render_label.get(path, dict())
    label = None
    if key in render:
        label_formatter = render[key]
        if type(label_formatter) == dict:
            if isinstance(value, Iterable) and not isinstance(value, str):
                label = ", ".join(format_value(label_formatter.get(v, v)) for v in value)
            else:
                label = format_value(label_formatter.get(value, value))
        else:
            key = label_formatter
    if label is None:
        if value is not None:
            value = format_value(value)
            label = "%s: %s" % (key, value) if not tooltip else value
        else:
            label = key
    return label


def render_box_properties(hurtbox, path, property_diff, additional_class, uk, projectile=False, **kwargs):
    all_properties = sorted(
        (k, v) for k, v in hurtbox.items() if k not in {
            "Height", "X", "Y", "Width", "TickStart", "TickEnd", "Unknown", "Parameters",
            "ActiveOnStates", "InactiveOnStates", "ActivateStates"
        })
    if "Command" in hurtbox and hurtbox["Command"] == "Spawn Projectile":
        projectile = True
    rendered_properties = [render_box_property(k, v, path, property_diff, additional_class, projectile=projectile, **kwargs) for k, v in all_properties]
    rendered_properties += [render_box_property(k, v["change"], path, property_diff, "removed", projectile=projectile, **kwargs) for k, v in property_diff.get("#DIFF", {}).items() if "removed" in v["type"]]
    if "Unknown" in hurtbox:
        property_diff = property_diff.get("Unknown", {})
        rendered_properties += [render_box_property(k, v, path, property_diff, additional_class, uk=True, projectile=projectile, **kwargs) for k, v in sorted(hurtbox["Unknown"].items())]
        rendered_properties += [
            render_box_property(k, v["change"], path, property_diff, "removed", uk=True, projectile=projectile, **kwargs)
            for k, v in property_diff.get("#DIFF", {}).items() if "removed" in v["type"]
        ]
    if "Parameters" in hurtbox:
        property_diff = property_diff.get("Parameters", {})
        rendered_properties += [render_box_property(k, v, path, property_diff, additional_class, uk, projectile=projectile, **kwargs) for k, v in sorted(hurtbox["Parameters"].items())]
        rendered_properties += [
            render_box_property(k, v["change"], path, property_diff, "removed", uk, projectile=projectile, **kwargs)
            for k, v in property_diff.get("#DIFF", {}).items() if "removed" in v["type"]
        ]
    for p in ["ActiveOnStates", "InactiveOnStates", "ActivateStates"]:
        if p in hurtbox:
            rendered_properties.append(render_box_property(p, hurtbox[p], path, property_diff, additional_class, uk, projectile=projectile, **kwargs))
    return "".join(rendered_properties)


def render_all_timeline(script, paths, diff, states, **kwargs):
    render = ""
    x_scale = 30
    y_space = 30
    max_tick = script["TotalTicks"]

    i = 1
    # spacer
    render += "<div class='timeline-header-spacer'></div>"
    for path in paths:
        to_draw = script.get(path)
        path_diff = diff.get(path, {})
        if not to_draw and not path_diff:
            continue
        uk = False
        if path == "Unknown":
            uk = True
            to_draw = to_draw.get("Others")
            if not to_draw:
                continue
            path_diff = path_diff.get("Others", {})
        added_items = {key for key, change in path_diff.get("#DIFF", {}).items() if change["type"] == "iterable_item_added"}
        removed_items = [
            (k, change["change"])
            for k, change in path_diff.get("#DIFF", {}).items()
            if change["type"] == "iterable_item_removed"
        ]

        # render items in order, the first part of the tuple is a sorting key
        # for old items put their actual key
        items = [(-n - 1, n, h) for n, h in removed_items]
        latest_delta = None
        for n, h in enumerate(to_draw or []):
            if n in added_items:
                # for new item, put their potential index
                items.append((n - (latest_delta or 0), n, h))
            else:
                # for old item, put their old key
                latest_delta = path_diff.get(n, {}).get("#DIFF", {}).get("#", {}).get("change", {}).get("delta", 0)
                items.append((n-latest_delta, n, h))

        title_block = "<div class='timeline-header%s' style='height:%dpx'><em>%s</em></div>" % (
            " unknown" if uk else "", (len(items)+1) * y_space, path
        )
        render += title_block
        i += 1
        for sorting_key, n, h in sorted(items):
            if "ActiveOnStates" in h:
                states.update(h["ActiveOnStates"])
            if "InactiveOnStates" in h:
                states.update(h["InactiveOnStates"])
            render += generate_timeline_item(h, i, n, path, path_diff, "removed" if n < 0 else ("added" if n in added_items else None), uk, x_scale, y_space, **kwargs)
            i += 1

    container = "<div style='position: relative;width:%dpx;'>%s</div>" % (
        max_tick * x_scale, render
    )
    return container


def generate_timeline_item(h, i, n, path, path_diff, additional_class, uk, x_scale, y_space, **kwargs):
    coordinates = "left:%dpx;top:%dpx" % (
        h["TickStart"] * x_scale, i * y_space
    )
    block_style = ["timeline"]
    box_id = "tl-%s-%d" % (path.lower(), n)
    property_diff = path_diff.get(n, {}).get("#DIFF", {})
    if uk:
        block_style.append("unknown")
    tooltip = []
    if additional_class:
        block_style.append(additional_class)
    if "TickStart" in property_diff:
        block_style.append("start-changed")
        tooltip.append("start: %d" % (property_diff["TickStart"]["change"]["old_value"]+1))
    if "TickEnd" in property_diff:
        block_style.append("end-changed")
        tooltip.append("end: %d" % (property_diff["TickEnd"]["change"]["old_value"]))
    if "X" in property_diff or "Y" in property_diff or "Width" in property_diff or "Height" in property_diff:
        block_style.append("shape-changed")
        tooltip.append("shape changed")
    block = "<div id='%s' class='%s' style='%s;width:%dpx;'%s></div>" % (
        box_id, " ".join(block_style), coordinates, (h["TickEnd"] - h["TickStart"]) * x_scale,
        " data-toggle='tooltip' data-placement='top' title='was %s'" % ", ".join(tooltip) if tooltip else ""
    )
    properties = "<div class='property-container%s' style='%s'>%s</div>" % (
        " unknown" if uk else "", coordinates, render_box_properties(h, path, path_diff.get(n, {}), additional_class, uk, **kwargs)
    )
    return block + properties
