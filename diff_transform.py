import json
import re


def get_from_path(path, content):
    if not path:
        return content
    try:
        return get_from_path(path[1:], content[path[0]])
    except:
        return None


def hash_change(obj):
    return json.dumps(obj, default=lambda x: "")


def split_path(path):
    return [eval(elem) for elem in re.findall("\\[([^\\]]*)\\]", path)]


def transform_diff(char_diff):
    result = dict()
    for diff_type, changes in char_diff.items():
        if type(changes) == set:
            changes = {c: True for c in changes}
        for path, change in sorted(changes.items()):
            path = split_path(path)
            if path[:2] == ["BCM", "Moves"] and path[-1] == "Index":
                continue
            to_change = result
            for p in path[:-1]:
                if p not in to_change:
                    to_change[p] = dict()
                to_change = to_change[p]
            if "#DIFF" not in to_change:
                to_change["#DIFF"] = dict()
            if path[-1] == "Index":
                change = {"delta": change["new_value"] - change["old_value"]}
            to_change["#DIFF"][path[-1]] = {
                "type": diff_type, "key": path[-1], "change": change
            }
    return result
