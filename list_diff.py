import re
from deepdiff import DeepDiff

from diff_transform import split_path, get_from_path
from copy import deepcopy


def compute_list_diff(t1, t2, diff_path, final_diff):
    changes = dict()
    found_target = set()
    found_source = set()

    for i, h1 in enumerate(t1):
        for j, h2 in enumerate(t2):
            if h2 == h1:
                # if i != j:
                #    changes[j] = {"values_changed": {"root['#']": {'delta': j-i}}}
                found_target.add(j)
                found_source.add(i)
                break
    for i, h1 in enumerate(t1):
        best_match = None
        if i in found_source:
            continue
        for j, h2 in enumerate(t2):
            if j in found_target:
                continue
            distance = 0
            tests = 0
            all_keys = (set(h2.keys()) | set(h1.keys())) - {"TickStart", "TickEnd"} - {"Parameters"}
            for k in all_keys:
                tests += 1
                if h2.get(k) != h1.get(k):
                    distance += 1
            if "Parameters" in h1 and "Parameters" in h2:
                param1 = h1["Parameters"]
                if "Params" in param1:
                    param1 = {i: v for i, v in enumerate(param1)}
                param2 = h2["Parameters"]
                if "Params" in param2:
                    param2 = {i: v for i, v in enumerate(param2)}
                for k, v in param1.items():
                    tests += 1
                    if param2.get(k) != v:
                        distance += 1
            elif ("Parameters" in h1) != ("Parameters" in h2):
                distance += 1
            # print(diff_path, i, j, distance, len(all_keys) / 3, all_keys)
            if distance > max(len(all_keys) / 3, 1):
                continue
            distance += abs(j - i) / 2
            distance += (h1.get("TickStart") != h2.get("TickStart")) * 1 + (h1.get("TickEnd") != h2.get("TickEnd")) * 1

            # print("\tmatch",distance)
            if best_match is None or best_match[0] > distance:
                best_match = distance, j
        if best_match:
            j = best_match[1]
            delta = DeepDiff(h1, t2[j])
            if i != j:
                if "values_changed" not in delta:
                    delta["values_changed"] = {}
                delta["values_changed"]["root['#']"] = {'delta': j - i}
            changes[j] = delta
            found_target.add(j)
            found_source.add(i)
        else:
            changes[-i - 1] = {"iterable_item_removed": {"root": h1}}
    for j, h2 in enumerate(t2):
        if j in found_target:
            continue
        changes[j] = {"iterable_item_added": {"root": h2}}
    diff_path = "root[" + "][".join("'%s'" % p if type(p) == str else str(p) for p in diff_path) + "]"
    for k, diff_change in changes.items():
        for diff_type, content in diff_change.items():
            if diff_type not in final_diff:
                final_diff[diff_type] = type(content)()
            if type(content) == dict:
                for path, result in content.items():
                    final_diff[diff_type][diff_path + "[" + str(k) + "]" + path[4:]] = result
            else:
                for path in content:
                    final_diff[diff_type].add(diff_path + "[" + str(k) + "]" + path[4:])
    return final_diff, found_target, found_source, changes


def find_in_diff(current_diff, search, prefix_size):
    return {tuple(split_path(path)[:prefix_size]) for diff_content in current_diff.values() for path in diff_content if
            re.search(search, path)}


def patch_diff(current_diff, v1, v2, search, prefix_size):
    for path in find_in_diff(current_diff, search, prefix_size):
        before = get_from_path(path, v1) or []
        after = get_from_path(path, v2) or []
        prefix_path = "root[" + "][".join("'%s'" % p if type(p) == str else str(p) for p in path) + "]"
        for diff_content in current_diff.values():
            if type(diff_content) == dict:
                for p in list(diff_content.keys()):
                    if p.startswith(prefix_path):
                        del diff_content[p]
            else:
                for p in list(diff_content):
                    if p.startswith(prefix_path):
                        diff_content.remove(p)
        compute_list_diff(before, after, path, current_diff)


def clean_diff(current_diff, v1, v2):
    current_diff = deepcopy(current_diff)
    patch_diff(current_diff, v1, v2, "BAC.*Hitboxes", 5)
    patch_diff(current_diff, v1, v2, "BAC.*Hurtboxes", 5)
    # patch_diff(current_diff, v1, v2, "BAC.*Positions", 5)
    patch_diff(current_diff, v1, v2, "BAC.*Cancels", 5)
    patch_diff(current_diff, v1, v2, "BAC.*Pushboxes", 5)
    patch_diff(current_diff, v1, v2, "BAC.*AutoCancels", 5)
    patch_diff(current_diff, v1, v2, "BAC.*Commands", 5)
    patch_diff(current_diff, v1, v2, "BAC.*Forces", 5)
    patch_diff(current_diff, v1, v2, "BAC.*Status", 5)
    patch_diff(current_diff, v1, v2, "BAC.*Others", 6)
    # patch_diff(current_diff, v1, v2, "BCM.*Moves", 2)
    # patch_diff(current_diff, v1, v2, "BCM.*CancelLists", 2)
    return current_diff
