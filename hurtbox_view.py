from list_diff import compute_list_diff


def get_hurtbox_icons(item):
    icons = []
    if item["Unknown6"] & 4:
        icons.append("/A")
    if item["Flag1"] & 4:
        icons.append("🖐")
    else:
        if item["Flag1"] & 1:
            icons.append("🤜")
        if item["Flag1"] & 2:
            icons.append("★")
    return icons


def draw_timeline_item(item, y, color, diff):
    style = 'stroke: %s' % color
    changed_style = 'stroke: orange'
    res = "<line x1='%d' y1='%d' x2='%d' y2='%d' style='stroke-width:1;%s'/>" % (
        item["TickStart"] * 50 + 1, y + 15, item["TickEnd"] * 50 + 1, y + 15, style
    )
    res += "<line x1='%d' y1='%d' x2='%d' y2='%d' style='stroke-width:3;%s'/>" % (
        item["TickStart"] * 50 + 1, y - 5, item["TickStart"] * 50 + 1, y + 15, style
        if "root['TickStart']" not in diff else changed_style
    )
    res += "<line x1='%d' y1='%d' x2='%d' y2='%d' style='stroke-width:3;%s'/>" % (
        item["TickEnd"] * 50 + 1, y - 5, item["TickEnd"] * 50 + 1, y + 15, style
        if "root['TickEnd']" not in diff else changed_style
    )
    res += draw_icons(get_hurtbox_icons(item), color, item["TickStart"] * 50 + 1, y)
    return res


def draw_grid(x_min, x_max):
    return "".join([
        "<line x1='%d' y1='0' x2='%d' y2='300' style='stroke-width:1;stroke: lightgray;'/>" % (
            i * 50 + 1, i * 50 + 1
        )
        for i in range(x_min, x_max)
    ])


def draw_icons(icons, color, x, y):
    res = ""
    for i, ic in enumerate(icons):
        neg = ""
        if ic[0] == '/':
            ic = ic[1:]
            neg = "<line x1='%d' x2='%d' y2='%d' y1='%d' stroke='black' stroke-width='1.5'></line>" % (
                x + 5 + i * 16, x + 15 + i * 16, y, y + 10
            )
        res += "<circle r='8' cx='%d' cy='%d' fill='white' stroke='%s'></circle>" % (
            x + 10 + i * 16, y + 5, color
        ) + "<text text-anchor='middle' dominant-baseline='central' x='%d' y='%d'>%s</text>" % (
            x + 10 + i * 16, y + 4, ic
        ) + neg
    return res


def draw_hurtbox_diff(previous, current):
    _, found_source, found_target, changes = compute_list_diff(previous, current, [], dict())
    all_changed_boxes = [b for i, b in enumerate(previous) if (-i-1) in changes] + [b for i, b in enumerate(current) if i in changes]
    if not all_changed_boxes:
        return ""
    #min_tick = min(b["TickStart"] for b in all_changed_boxes)
    max_tick = max(b["TickEnd"] for b in all_changed_boxes) + 1

    grid = draw_grid(0, max_tick)
    timeline = ""
    for i, h in enumerate(previous):
        timeline += draw_timeline_item(h, 30 * i + 5, 'red' if i not in found_target else 'black',
                                       changes.get(-i - 1, dict()).get("values_changed", dict()))
    before_html = "<h3>Before</h3>" + \
                  "<svg width='%d' height='%d'>" % (max_tick * 50, len(previous) * 30 + 10) + grid + timeline + "</svg>"

    timeline = ""
    for i, h in enumerate(current):
        timeline += draw_timeline_item(h, 30 * i + 5, 'green' if i not in found_source else 'black',
                                       changes.get(i, dict()).get("values_changed", dict()))
    after_html = "<h3>After</h3>" + \
                 "<svg width='%d' height='%d'>" % (max_tick * 50, len(current) * 30 + 10) + grid + timeline + "</svg>"
    return before_html + after_html
